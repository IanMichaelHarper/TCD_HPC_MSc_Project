import matplotlib.pyplot as plt
import numpy as np

#init list with number of processors
#rank = [3,5,7,9,16] #with datapoint for 16 proces
rank = [3,5,7,9] #without

#init lists with runtimes
runtimes200 = [np.mean([52.379342, 14.052725, 9.508005, 67.454895]), np.mean([89.065740, 89.624976, 89.071479, 75.191175]), np.mean([94.202972, 94.210269, 89.007963, 102.250697]), 115.763523, 157.205384] #with 16 proc time
#runtimes200 = [np.mean([52.379342, 14.052725, 9.508005, 67.454895]), np.mean([89.065740, 89.624976, 89.071479, 75.191175]), np.mean([94.202972, 94.210269, 89.007963, 102.250697]), 115.763523] #without
runtimes2000 = [1107.488322, 1750.651, np.mean([2282.352166, 2282.635017]), 3010.439]

#plot hard scaling graph for 2000 particles
plt.plot(rank, runtimes2000)
plt.xlabel("Number of processors");
plt.ylabel("runtimes in seconds")
plt.title("runtimes for a single simulation of 2000 particles in parallel")
plt.savefig("2000hard_scaling_graph.png")

#plot hard scaling graph for 200 particles with 16 proc datapoint
rank.append(16);
plt.figure()
plt.plot(rank, runtimes200)
plt.xlim(xmin=3)
plt.xlabel("Number of processors");
plt.ylabel("runtimes in seconds")
plt.title("runtimes for a single simulation of 200 particles in parallel")
plt.savefig("200hard_scaling_graph.png")
