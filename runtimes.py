import matplotlib.pyplot as plt
import numpy as np

SingleSerial = []
SingleSerial.append([0.65,0.7,0.66,0.68 ])
SingleSerial.append([0.17,1,0.4,1.2,0.3])
SingleSerial.append([4.67,5, 5.5,  10.311197, 5,5.6,4.59 ]) #have another datapoint here, from notes: "A single simulation of 200 particles takes ~5seconds +- 0.5 seconds (roughly by eye looking at the last handful)"
SingleSerial.append([27.328713,25.16,26.16,26.8,25,26.532,26.78 ])
SingleSerial.append([67,66.668559,66.5,66.28,66])
SingleSerial.append([1824,1781.944833,1825,1816,1829,1757.930627 ])
Ensemble50Serial20 = [7.2244,8,7.3,7.4,7.66]
Ensemble500Serial20 = [81.168729]
Ensemble5000Serial20 = [813.522773,817.764953, 813.522773]
Ensemble50Serial200 = [250.523,  561.391886]
Ensemble500Serial200 = [2517.141531,2547.709799]
Ensemble50Serial400 = [ 813.522773]
Ensemble50Serial600 = [3362.194298]
Ensemble5000Serial80 = [3362.194298]

print np.mean([1824,1781.944833,1825,1816,1829,1757.930627 ])

#could change arrays to 2d arrays where each row is number of particles, then could set N array by writing the number of particles (20, 200 etc) to it the a number of times depending on how many datapoints we have

#something like
#N = [num_particles for num_particles in 2D_single[row]]

#2D_single = [SingleSerial20, SingleSerial80, SingleSerial200, SingleSerial400, SingleSerial600, SingleSerial2000]

num_diff_N = 6 #the number of different particle numbers (i.e. some sims have N=20, others have N=80 etc.)
N = []
N.append([20 for t in SingleSerial[0]])
N.append([80 for t in SingleSerial[1]])
N.append([200 for t in SingleSerial[2]])
N.append([400 for t in SingleSerial[3]])
N.append([600 for t in SingleSerial[4]])
#N.append([2000 for t in SingleSerial[5]]) #this datapoint skews the graph so that its hard to see the exponential rise, but it would be good to plot a graph both with and without it to show how significantly runtime increases for large number of particles

for n,ls in enumerate(N):
	plt.plot(ls, SingleSerial[n], 'o')
plt.title("runtimes of a single simulation in serial against number of particles N")
plt.ylabel("runtime (in seconds)")
plt.xlabel("number of particles N")
plt.savefig("Runtimes_vs_N_without2000.png")

finalSerial = []
finalParallel = []
finalN = [20]
finalSerial.append(0.850213)
finalParallel.append(5.585290)
finalN.append(80);
finalSerial.append(0.063802)
finalParallel.append(6.05188118)
finalN.append(140)
finalSerial.append(1.833699)
finalParallel.append(7.17828368)
finalN.append(160)
finalSerial.append(2.684044)
finalParallel.append(7.7047611)
finalN.append(200)
finalSerial.append(5.334953)
finalParallel.append(8.866106180000001)
finalN.append(400)
finalSerial.append(23.642629)
finalParallel.append(21.25697502)
finalN.append(600)
finalSerial.append(67.518592)
finalParallel.append(59.042999)
plt.figure()
plt.plot(finalN, finalSerial, label="Serial");
plt.plot(finalN, finalParallel, label="Parallel")
plt.legend()
plt.xlabel("Number of Particles N")
plt.ylabel("Runtime in seconds")
plt.title("Runtimes for a Single Simulation in Serial and Parallel for 3 processors")
plt.savefig("serial_v_parallel_short.png")
finalN.append(2000)
finalSerial.append(1825.64591)
finalParallel.append(1129.086289)
finalN.append(2500)
finalSerial.append(4031.662903)
finalParallel.append(2652.350496)
finalN.append(3000)
finalSerial.append(6887.871637)
finalParallel.append(3583.983095)

plt.figure()
plt.plot(finalN, finalSerial, label="Serial");
plt.plot(finalN, finalParallel, label="Parallel")
plt.legend()
plt.xlabel("Number of Particles N")
plt.ylabel("Runtime in seconds")
plt.title("Runtimes for a Single Simulation in Serial and Parallel for 3 processors")
plt.savefig("serial_v_parallel_long.png")
