#include "header.h"

//20 particles confined in a shallow, one-dimensional
//harmonic well of stiffness k0 under conditions of underdamping
//viscous drag, low temperature, and strong, sinusoidal
//forcing [16,17] of a single particle (labeled with index 0)

int main(int argc, char * argv[]){
	//init random seed
	srand(time(NULL));
	
	//init MPI
	int rank,size;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Status stat;	

	//alloc memory	
	double * x = calloc(N, sizeof(double)); //start at origin //two ghost rows for middle of array
	double * xdot = calloc(N, sizeof(double)); //start at origin
	double * x_double_dot = calloc(N, sizeof(double)); //start at origin
	int ** A = malloc(N*sizeof(double *)); //Adjacency matrix
	int * Adata = calloc(N*N,sizeof(double));
	int ** L = malloc(N*sizeof(double *)); //Graph laplacian matrix
	int * Ldata = calloc(N*N,sizeof(double));
	double *z,*f,*a,*wr,*wi,*w,*vl,*vr,*pos; //eigenvars and pos
	if (rank == 0){	
		z = calloc(N, sizeof(double)); //start at origin
		f = calloc(N, sizeof(double)); //"normal mode" force
		a = calloc(N*N,sizeof(double)); //make new array for L matrix since a is overwritten by dgeev()	
		wr = calloc(N,sizeof(double));//real part of eigenvalues
		wi = calloc(N,sizeof(double)); //imaginary part of eigenvalues
		w = calloc(N,sizeof(double)); //eigenvalues
		vl = calloc(N*N,sizeof(double)); //left eigenvectors
		vr = calloc(N*N,sizeof(double)); //right eigenvectors
		pos = calloc(N, sizeof(double)); //positions of particles stored for writing to file
	}
	int rough_std_dev = 6; //particles will be in [-6,6] 68% of the time
	for (int i=0; i<N; i++){
		//x[i] = rand() % (10+rank*2); //start at random locations - rank term to ensure all particiles have unique starting points
		//x[i] = i-10; //start equally spaced in pot well
		x[i] = not_here; //signifies data on other processor

		//init positions evenly spaced out in potential well, with spatially decomposed proc grid
		if (i-10 <=-rough_std_dev){
			if (rank == 0){
				x[i] = i-10;
				pos[i] = i-10;
			}
		}
		else if (i-10 > -rough_std_dev && i-10 < rough_std_dev){
			if (rank == 1){
				x[i] = i-10;
				MPI_Send(&x[i],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD); 
			}
			else if (rank == 0)
				MPI_Recv(&pos[i],1,MPI_DOUBLE,1,0,MPI_COMM_WORLD,&stat);
		}
		else{
			if (rank == 2){
				x[i] = i-10;
				MPI_Send(&x[i],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD); 
			}
			else if (rank == 0)
				MPI_Recv(&pos[i],1,MPI_DOUBLE,2,0,MPI_COMM_WORLD,&stat);
		}
		A[i] = &Adata[i*N];
		L[i] = &Ldata[i*N];
		MPI_Barrier(MPI_COMM_WORLD);
	}
	print_particle_positions(x); //starting positions
	if (rank == 0){
		printf(" ------------- pos ---------------\n");
		for (int i = 0; i < N; i++)
			printf("%lf ", pos[i]);
		printf("\n");
	}
	if (rank == 0){ //serialize for now
		setA(pos,A);
		setL(A,L);
	}
	MPI_Bcast(&A[0][0], N*N, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&L[0][0], N*N, MPI_INT, 0, MPI_COMM_WORLD);

	FILE * positions,*results,*all_evals,*g,*bf;
	if (rank == 0){
		init_files(positions,results,all_evals,g,bf);
	}
	MPI_Barrier(MPI_COMM_WORLD);
	
	//init vars
	struct timeval start, end; //for timing
	long long time_taken;
	int t = 0; //time 
	int dt = 1; //change in time between iterations
	double kL_sum, all_procs_kL_sum;
	double temp;	
	int flag = false;
	int src,dest;
	
	//time loop
	int num_iters = 10000; //num steps in paper
	gettimeofday(&start,NULL);
	while (t < num_iters){
#if 0 
		if (t <= 1) {
			printf("particle positions for iteration %d:\n",t);
			print_particle_positions(x);
		}
		else if (t > 5) exit(1);
#endif
		if ((t % 50) == 0){ //t % temp_timelimit/10 doesn't work here?
			if (rank == 0){
				printf("t = %d\n", t); //ideally want progress bar here
				print_particle_positions(pos);
			}
		}
//		dt = log(1/drand48())/r(i,j,x,A); //Gillespie's Algo
		for (int i=0; i<N; i++){
			//if (x[i] > 84 && x[i] != not_here){
			kL_sum = 0;
			for (int j=0; j<N; j++){
				if (abs(x[j]) > 500 && x[j] != not_here){
					printf("before kL_sum = %d*%d*%lf = %d\n",k,L[i][j],x[j], kL_sum);
					printf("rank = %d, i = %d, j = %d, t = %d\n",rank,i,j,t);
					printf(" exiting on line %d\n", __LINE__);
					exit(1);
				}
				if (x[j] != not_here){ //x array changes after this statement - prob buffer overflow
#if 0					
					if (t == 1 && j == 0 && i == 0){
						printf("particle positions on rank %d for iteration %d before kL sum:\n", rank,t);
					//	print_particle_positions(x);
						printf("x[%d] = %lf on rank %d\nL[%d][%d] = %d at t = %d on rank %d\n",j,x[j],rank,i,j,L[i][j],t, rank); 
						printf("\n");
					}
					if (t == 0 && i == 0 && j == 0) printf("calculating kl_sum on rank %d\n", rank);
#endif
					kL_sum -= k*L[i][j]*x[j];
					//printf("after kL_sum = %d*%d*%lf = %d\n",k,L[i][j],x[j], kL_sum);
					if (abs(x[j]) > 10000 || abs(L[i][j]) > 10000){ 
						printf("current rank = %d\n", rank);
						printf("kL_sum = %d*%d*%lf = %d\n",k,L[i][j],x[j], kL_sum);
						print_particle_positions(x); 
						printf("x[%d] = %lf on rank %d\n L[%d][%d] = %d at t = %d on rank %d\n",j,x[j],rank,i,j,L[i][j],t, rank); 
						printf(" exiting on line %d\n", __LINE__);
						exit(1); 
					}
				}
			}
			MPI_Allreduce(&kL_sum, &all_procs_kL_sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
#if 0
			if (kL_sum != 0){
				for (int root = 0; root < size; root++){
					if (rank == root){ 
						printf("i = %d: kl sum = %d on rank %d\n",i,kL_sum, rank);
						printf("i = %d: all procs kl sum = %d on rank %d\n",i, all_procs_kL_sum, rank);
					}
					MPI_Barrier(MPI_COMM_WORLD);
				}
			}
			if (t % 50 == 0 && i % 5 == 0) printf("all_proc_kl_sum = %d on rank %d\n", all_procs_kL_sum, rank);
#endif
			//Equation of Motion calculation: a = F/m
			if (x[i] != not_here){ //could try put safeguard here to give error if no or more than 1 process enters this if statement
				if (i == 0) //driving force only applied to particle 0
					x_double_dot[i] = (all_procs_kL_sum - b*xdot[i] + F*sin(omega_d * t) - k0*x[i])/m;
				else 
					x_double_dot[i] = (all_procs_kL_sum - b*xdot[i] - k0*x[i])/m;
				//update time and particle positions and velocities for next iteration
				xdot[i] += x_double_dot[i] * dt; //dt here is the change in time between this iteration and nex iteration
				if (xdot[i] == not_here){ //for whatever reason
					printf("\n XDOT[I] == NOT_HERE, X[I] WILL BE OUTSIDE RANGE IN LAST ELSE STATEMENT IN EOM CALCULATION \n");
					exit(1);
				}
				x[i] += xdot[i] * dt;
				//change x array to accommodate spatial decomposition
				//new positions are taken from non-null processors and given to processors allocated to that region in pot well
				if (x[i] <=-rough_std_dev){
//					printf("rank %d has x[i] <= -6\n", rank);
					temp = x[i]; //get value from all proc with non-null value
					x[i] = not_here; //replace this processors value of x[i] with not_here
					if (rank == 0) 
						x[i] = temp; //give to proc 0
				}
				else if (x[i] > -rough_std_dev && x[i] < rough_std_dev){
//					printf("rank %d has x[i] in (-6,0]\n", rank);
					temp = x[i]; //get value from all proc with non-null value
					x[i] = not_here; //replace this processors value of x[i] with not_here
					if (rank == 1)
						x[i] = temp; //give to proc 1
				}
				else{ //this might cause problems since not_here gets printed as a very large number
//					printf("rank %d has x[i] >= 6, value is %lf\n", rank, x[i]);
					temp = x[i]; //get value from all proc with non-null value
					x[i] = not_here; //replace this processors value of x[i] with not_here
					if (rank == 2)
						x[i] = temp; //give to proc 3 
				}
				//update pos array on rank 0
				flag = false;
				if (rank == 0)
					pos[i] = x[i];
				else{
					flag = true; //indicates rank 0 must recv
					MPI_Send(&x[i], 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD); //more comm - bottleneck **should change to Isend
				}
				if (flag == true && rank == 0)
					MPI_Recv(&pos[i], 1, MPI_DOUBLE,MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &stat);
				
	//			MPI_Waitall(2,reqs,stats);
			}
			MPI_Barrier(MPI_COMM_WORLD);

			//x_double_dot[i] = (kL_sum - b*xdot[i] - k0*x[i])/m; //non-driven system
		}
//		printf("rank %d got EOM calc\n", rank);
		MPI_Barrier(MPI_COMM_WORLD);
//		printf("rank %d got before sendrecv\n", rank);
#if 0
		if (t == 0){
			print_particle_positions(x);
			exit(1);
		}
#endif
/*		printf("rank %d got after Power func\n", rank);
		for (int proc = 0; proc < size; proc++){
			if (rank == proc){
				printf("rank %d matrix:\n", rank);
				print_matrix(L);
			}
			MPI_Barrier(MPI_COMM_WORLD);
		}
*/		//pre-eigen assignment	
		for (int i=0; i<N; i++){
			for (int j=0; j<N; j++){ //L is symmetric so can cut this loop in half
				flag = true; //assume desired L[i][j] is not on rank 0
				if (rank == 0){ //since we are sending to rank 0 no need to send info here
					a[i*N + j] = L[i][j];
					flag = false; //flag to prevent rank 0 receiving
				}
				/*else{
				//	printf("rank %d is about to send L[%d][%d] = %d\n", rank, i, j, L[i][j]);
					MPI_Send(&L[i][j],1,MPI_INT,0,0,MPI_COMM_WORLD); //send L value to rank 0
				}
				//printf("rannk %d is waiting at barrier\n");
				if (rank == 0 && flag == true)
					MPI_Recv(&a[i*N + j],1,MPI_DOUBLE,MPI_ANY_SOURCE,0,MPI_COMM_WORLD,&stat); //rank 0 receives data - would like more precise way of knowing source proc instead of MPIANY_SOURCE
			*/	MPI_Barrier(MPI_COMM_WORLD);
			//	printf("rannk %d is waiting at barrier\n");
			//	printf("rannk %d is past barrier\n");
			}
		}
		//printf("rank %d got before eigensolve\n", rank);
		if (rank == 0){
#if 0
			if (t % 50 == 0){
				printf("--------------------a matrix --------------------------\n");
				for (int i=0; i<N; i++){
					for (int j=0; j<N; j++){
						printf("%lf ", a[i*N + j]);
					}
					printf("\n");
				}
				printf("\n");
			}
#endif
			//eigencalulation	
			lapack_int n=N, lda = N, ldvl = N, ldvr = N;
			lapack_int info = LAPACKE_dgeev(LAPACK_ROW_MAJOR,'N','V',n,a,lda,wr,wi,vl,ldvl,vr,ldvr); //get eigenvars
			
			//get normal mode displacements z
			for (int i=0; i<N; i++){
				f[i] = F*vr[0*N + i];
				z[i] = (f[i]*cexp(I*omega_d*t))/(k0*wr[i] - m*pow(omega_d,2) + I*b*omega_d); //k0 could be k here, not sure - possibly equivalent
			}

			//write particle positions to file for plotting
			positions = fopen("positions.csv", "a");
			fprintf(positions, "%d", t);
			for (int i=0; i<N; i++){
				fprintf(positions, ",%lf", pos[i]);
			}
			fclose(positions);

			all_evals = fopen("all_evals.csv", "a");
			fprintf(all_evals, "%d", t);
			for (int i=0; i<N; i++){
				fprintf(all_evals, ",%lf",wr[i]);
			}
			fprintf(all_evals, ",%lf\n", mu(wr));
			fclose(all_evals);
		}
		MPI_Barrier(MPI_COMM_WORLD);
//		if (t % 50 == 0)
//			printf("rank %d got to code after write files\n", rank);
		if (rank == 0){ //serialize for now
			setA(pos,A);
			setL(A,L);
		}
		MPI_Bcast(&A[0][0], N*N, MPI_INT, 0, MPI_COMM_WORLD);
		MPI_Bcast(&L[0][0], N*N, MPI_INT, 0, MPI_COMM_WORLD);
		
		t += dt;
//		printf("particle positions for t = %d:\n",t);
//		print_particle_positions(x,size,rank);
	}
	printf("rank %d waiting at barrier after while loop\n", rank);
	MPI_Barrier(MPI_COMM_WORLD);
	gettimeofday(&end,NULL);
	print_particle_positions(x);
	double div = 1000000;
	time_taken = (end.tv_sec - start.tv_sec)*100000L + (end.tv_usec - start.tv_usec);
	double time_in_usec = (double)time_taken;
	double time_taken_in_seconds = time_in_usec/div;
	printf("simulation took %lf seconds\n", time_taken_in_seconds);

	free(x);
	free(xdot);
	free(x_double_dot);
	free(A[0]);//might not need this line after free(U[[0])
	free(A);
//	free(bond_rates);
	if (rank == 0){
		free(z);
		free(f);
		free(w);
		free(wr);
		free(wi);
		free(a);
		free(vl);
		free(vr);
		free(pos);
	}
	printf("rank %d got between frees and MPI_Finalize()\n", rank);

	MPI_Finalize();	
	return 0;
}
