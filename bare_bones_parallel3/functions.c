#include "header.h"

//distance between particle i and j

//delta_x function used to be here

double delta_x(int i, int j, double * x){
	/*if (x[i] != 'NULL'){
		if (x[j] != 'NULL')
			return fabs(x[i] - x[j]);
	}*/
	return fabs(x[i] - x[j]);
}

//At each instant, a given pair of particles i and j separated by
//distance delta_x[i][j] either share a single bond of internal energy
//E or do not (in which case, E=0)

//E function used to be here

//for use in Arrhenius relation r - bond energy of particle?

//Ea function used to be here


//height of the
//transition state barrier separating the bonded and unbonded states

//B function used to be here

// the formation
// and breakage of bonds is assumed to follow a simple twostate
// stochastic switching process whose bonding and
// unbonding rates are governed by an Arrhenius [14] relation

//r function used to be here


// adjacency matrix defining the connectivity of our network
// of springs/bonds
void setA(double * x, int ** Adj){
	double r,dx,E,Ea,B;
	//loop over half of A only to ensure symmetric & same rate
	for (int i=0; i<N; i++){
		for (int j=0; j<i; j++){
			dx = fabs(x[i] - x[j]);
			
			//get bond energy
			if (Adj[i][j] == 1) //if particle i is bonded to particle j
				E = (k*dx*dx/2 - epsilon);//bond energy
			else if (Adj[i][j] == 0) //if i and j are unbonded
				E = 0;
			else {
				printf("entries of adjacency matrix A are not 1 or 0. Fix code. Exiting\n");
			exit(1);
			}
			//return (k*pow(delta_x(i,j,x), 2))/2 - epsilon;//bond energy

			B = k*dx*dx; //barrier height
			Ea = B - E; //diff
			r = rO * exp(-Ea/(boltzmann*T)); //rate of bond formation/breakage
			//might need to change this based on whether we already have a bond between i and j
			if (r > drand48()){ //Gillspie's Aglo
//			if (r(i,j,x,Adj) > 0) //if ith particle is bonded to the jth particle
				Adj[i][j] = 1;
				Adj[j][i] = 1;
			}
			else {
				Adj[i][j] = 0;
				Adj[j][i] = 0;
			}
		}
	}
}


//graph Laplacian matrix Lij depends on which particles are
//connected by bonds at each moment in time, and changes
//according to the sequence of reactions between particles
void setL(int ** A, int ** L){
//	printf(" in A\n");
	int sum,all_proc_sum;
	for (int i=0; i<N; i++){ 
		sum = 0;
		for (int l=0; l<N; l++){
//			if (A[i][l] != not_here)
				sum += A[i][l];
		}	
//		printf("before reduce\n");
//		MPI_Reduce(&sum, &all_proc_sum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
//		printf(" before bcast\n");
//		MPI_Bcast(&all_proc_sum,1,MPI_INT,0,MPI_COMM_WORLD);
//		printf("after bcast\n");
		for (int j=0; j<N; j++){
//			if (A[i][j] == not_here)
//				L[i][j] = not_here;
//			else{
				if (i == j)
					L[i][j] = sum - A[i][j];
				else
					L[i][j] = -A[i][j];
//			}
		}
	}
}
//total energy of system H = KE + PE = T + V
double H(double * x, double * xdot, int ** A){
	double Tsum = 0;
	double xsum = 0;
	double Adxsum = 0;
	for (int i=0; i<N; i++){
		Tsum += pow(xdot[i],2);
		xsum += pow(x[i],2);
		for (int j=0; j<i; j++){
			Adxsum += A[i][j]*delta_x(i,j,x);
		}
	}
	Tsum = 0.5*m*Tsum;
	xsum = 0.5*k0*xsum;
	Adxsum = 0.5*k*Adxsum;
	return Tsum + xsum + Adxsum;
}

// total work done on system in 1 iteration
// i.e. the work absorbed from driving force in 1 iteration
double W(int t, double * xdot){
	double sum = 0;
	int dt = 1; //should put as a function arg
//	for (int i=0; i<N; i++){
//		sum += F*sin(omega_d*t)*xdot[i]*dt; //this may not be correct for all non-driven particles. The force on non-driven particles that causes them to move might not be the full applied driving force, but a fracton of it applied indirectly though the bonds of the system
//	}
	sum = F*sin(omega_d*t)*xdot[0]*dt;
	return sum;
}

//transient work 
//double dW(double * xdot){

//average power dissipated
double P(double * x){
	double sum = 0;
	double all_proc_sum;
	for (int i=0; i<N; i++){
		if (x[i] != 'NULL')
			sum += abs(x[i]);
	}
	MPI_Reduce(&sum, &all_proc_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	return b*pow(omega_d,2)*all_proc_sum/2; //doesn't matter what other ranks return here since only rank 0 writes this data to file for plotting
}

//change in entropy between bond events
double delta_S(){
	//need to save info from last iteration to compute this
	return 0;
}

//Bessel function - taken from https://www.sciencedirect.com/science/article/pii/0898122181900808
double I0(double y){
	printf("y = %lf\n", y);
	int j = 1;
	double top, bottom, diff = 1, sum = 0, last_sum = 0;
	double tol = 0.0001; //accuracy too which infinite sum is taken
	while (diff > tol){
		top = pow(y*y/4,j);
		//printf("top = %lf\n", top);
		bottom = pow(tgamma(j+1),2);
		sum += top/bottom;
		diff = fabs(last_sum - sum);
		printf("diff = %lf\n", diff);
		last_sum = sum; //for next iter
		j++;
		if (isinf(sum) != 0){
			printf("top = %lf\n", top);
			printf("bottom = %lf\n", bottom);
			printf("sum = %lf\n", sum);
		}
	}
	return sum + 1;
}
		
//normal mode spectrum? "empirical spectral distribution of L"
//from https://projecteuclid.org/download/pdfview_1/euclid.aoap/1287494555
double mu(double * lambda){
	double sum = 0;
	for (int i=0; i<N; i++){
		if (lambda[i] == 0)
			sum += 1;
	}
	return (double)(sum/N);
}

void print_matrix(int ** A){
	printf("------------------------------- ");
	printf("%s ", getName(A));
	printf("matrix ----------------------------------------\n");
	for (int i=0; i<N; i++){
		for (int j=0; j<N; j++)
			printf("%d ", A[i][j]);
		printf("\n");
	}
	printf("\n");
}	

void print_particle_positions(double * x){
	printf("particle positions on rank %d:\n----------------------------------------\n[");
	for (int i=0; i<N; i++){
		printf(" %lf ", x[i]);
	}	
	printf("]\n\n");
}

void init_files(FILE *positions,FILE * results, FILE * all_evals, FILE * g, FILE * bf){
	//init files
	positions = fopen("positions.csv", "w");
	double pos_sum = 0;
	if (positions == NULL){
		printf("file is null, exiting");
		exit(1);
	}
	fprintf(positions,"time");
	for (int i=0; i<N; i++){
		fprintf(positions,",x[%d]", i);
	}
	fprintf(positions,",pos_sum");
	fprintf(positions,",avg_position");
	fprintf(positions,",std_dev\n");
	fclose(positions); 
	
	//power dissipated I think? - check	
	results = fopen("results.csv", "w");
	if ( results == NULL){
		printf("file is null, exiting");
		exit(1);
	}
	fprintf(results,"t,P1,P2\n");
	fclose(results);

	//eigenvalues
	all_evals = fopen("all_evals.csv", "w");
	if (all_evals == NULL){
		printf("file is null, exiting");
		exit(1);
	}
	fprintf(all_evals, "time");
	for (int i=0; i<N; i++){
		fprintf(all_evals, ",lambda[%d]", i);
	}
	fprintf(all_evals, ",mu\n");
	fclose(all_evals);
	
	//energy and bonds file init
	g = fopen("energy.csv", "w");
	fprintf(g,"time,H,W,Wtot\n");
	fclose(g);
	bf = fopen("bonds.csv", "w");
	fprintf(bf, "time,bonds per particle\n");
	fclose(bf);
}

#if 0	
void writefiles(FILE * positions, FILE * results, FILE * all_evals, FILE * g, FILE * bf){
		positions = fopen("positions.csv", "a");
		fprintf(positions, "%d", t);
		double pos_sum = 0; //reset for next iteration
		for (int i=0; i<N; i++){
			fprintf(positions, ",%lf", z[i]);
			pos_sum += abs(z[i]);
		}
		fprintf(positions, ",%lf\n", pos_sum);
		fclose(positions);

		all_evals = fopen("all_evals.csv", "a");
		fprintf(all_evals, "%d", t);
		for (int i=0; i<N; i++){
			fprintf(all_evals, ",%lf", lambda[i]);
		}
		fprintf(all_evals, ",%lf\n", mu(lambda));
		fclose(all_evals);
}
//write Laplacian matrix to file
void writeL2file(char * filename, int ** A){
	FILE * f = fopen(filename,"w");
	if (f == NULL) exit(-1);
	for (int i=0; i<N; i++){
		for (int j=0; j<N; j++)
			if (j == N-1)
				fprintf(f,"%d", L(i,j,A));
			else
				fprintf(f,"%d,", L(i,j,A));
		fprintf(f,"\n");
	}
//	fprintf("\n");
	fclose(f);
}
#endif

//print initial configuration to file for gephi visualiation
void write_graph(int ** A){
	int flag = 0;
	FILE * gephi = fopen("graph.csv", "w"); //graph for gephi visualization
	for (int i=0; i<N; i++){
		flag = 0;
		for (int j=0; j<i; j++){
			if (A[i][j] == 1){ //if nodes are connected by bond
				flag = 1;
				fprintf(gephi, "%d;%d\n",i,j); //i-j pair
			}
		}
		if (flag == 0)
			fprintf(gephi,"%d\n", i); //node not bonded
	}
	fclose(gephi);
}

//function to read eigenmatrix in filename and return eigenmatrix U
void read_eigenvectors(char * filename, double ** U){
	//init vars
//	double ** U = malloc(N * sizeof(double *));
//	double * Udata = malloc(N*N * sizeof(double));
	char * token;
	const char s[2] = ",";
	char * line = malloc(1024*sizeof(char));
//	for (int i=0; i<N; i++)
//		U[i] = &Udata[i*N];

	//open file
	FILE * f = fopen(filename, "r");
	if (f==NULL) exit(-1);

	//read file to get U
	int j = 0;
	for (int i=0; i<N; i++){
		fscanf(f,"%s", line);
		/* get the first token */
		token = strtok(line, s);
		/* walk through other tokens */
		j = 0;
		while( token != NULL ) {
			U[i][j] = atof(token); //assign U values
		//	printf("U[%d][%d] = %d\n",i,j,U[i][j]);
			token = strtok(NULL, s);
		//	printf("%dth token in the %dth line = %s\n",j,i,token);
			j++;
		}
	}
	//printf("%s\n", f);
	fclose(f);
	free(line);
#if 0 
	for (int i=0; i<N; i++){
		for (int j=0; j<N; j++)
			printf("%lf ", U[i][j]);
		printf("\n");
	}
#endif
}	
void read_eigenvalues(char * filename, double * lambda){
	//init vars
	char * token;
	const char s[2] = ",";
	char * line = malloc(1024*sizeof(char));

	//open file
	FILE * f = fopen(filename, "r");
	if (f==NULL) exit(-1);

	//read file to get lambda
	fscanf(f,"%s", line);
	/* get the first token */
	token = strtok(line, s);
	/* walk through other tokens */
	int j = 0;
	while( token != NULL ) {
		lambda[j] = atof(token); //assign U values
		token = strtok(NULL, s);
		j++;
	}
	//printf("%s\n", f);
	fclose(f);
#if 0
	for (int i=0; i<N; i++){
		printf("%lf ", lambda[i]);
	}
	printf("\n");
#endif
	free(line);
}	

/*
void get_bond_rates(double * x, double * bond_rates){
	int num_new_bond_rates = 0;
	for (int i=0; i<N; i++){
		for (int j=0; j<N; j++)
			for (int l=0; l<N*N; l++){
				if (r(i,j,x) 
			
}*/
