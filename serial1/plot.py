import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from math import isnan

#plot the number of bonds per particle against time
bonds = pd.read_csv("bonds.csv");
#plt.figure()
plt.plot(bonds["time"], bonds["bonds per particle"],'o');
plt.title("bonds per particle against time")
plt.xlabel("time t")
plt.ylabel("bonds per particle");
plt.savefig("nbonds_vs_time.png")

#plot particle positions against time
df1 = pd.read_csv("positions.csv")
count = 0
m = []
s = []
plt.figure()
for col in df1:
	if col == "time" or col == "pos_sum":
		continue;
	#if count % 5 == 0 and count != 0:
	#	plt.figure()
	#plt.title("positions of particles {0}-{1} against time".format(count-5, count));
	plt.plot(df1["time"], df1[col])
	m.append(df1[col].mean())
	s.append(df1[col].std())
	count += 1
plt.title("particle positions against time");
plt.xlabel("time t")
plt.ylabel("x(t)")
plt.savefig("x_vs_t.png")

print "mean = ", np.mean(m)
print "std = ", np.mean(s)

N = count-1 #number of particles


#plot Power P against time
df = pd.read_csv("results.csv");
plt.figure()
plt.plot(df["t"], df["P2"], 'x')
plt.title("Power dissipated against time")
plt.ylabel("Power dissipated P")
plt.xlabel("time t");
plt.savefig("P_vs_t.png")

#plot Power P at adjacent iterations
plt.figure()
print "plotting PvP"
plt.plot(df["P1"], df["P2"], 'o');
plt.xlabel("P(t_n)");
plt.ylabel("P(t_(n+1))");
plt.title("Power dissipated by system at adjacent iterations");
plt.savefig("PvP.png")
print "finished plotting PvP"

#plot H and W
Hdf = pd.read_csv("energy.csv");
plt.figure()
plt.plot(Hdf["time"], Hdf["H"])
plt.title("Total energy of system H against time");
plt.xlabel("Time t");
plt.ylabel("Energy H");
plt.savefig("H_vs_t.png");

df2 = pd.read_csv("all_evals.csv");

plt.figure()
plt.plot(Hdf["time"], Hdf["W"]);
plt.title("Work absorbed by system against time");
plt.xlabel("time t");
plt.ylabel("Work absorbed at each iteration");
plt.savefig("W_vs_t.png")
plt.figure()
plt.plot(Hdf["time"], Hdf["Wtot"]);
plt.title("total Work absorbed by system against time");
plt.xlabel("time t");
plt.ylabel("cumulative work absorbed")
plt.savefig("Wtot_vs_t.png")

#plot normal mode spectrum
plt.figure()
plt.plot(df2["time"], df2["mu"],'x')
plt.title("Normal Mode Spectrum P(w)");

rows = df2.shape[0]
cols = df2.shape[1]
print "rows = ", rows
print "cols = ", cols
sumd = 0
"""
P = []
#plt.figure()
for index, row in df2.iterrows():
	for col in df2:
		if col == "time":
			continue
		if row[col] == 0:
			sumd += 1
	P.append(sumd/rows)
	#	plt.plot(df2["time"], df2[col], 'x')
#P = np.array(P)
#P = np.zeros((rows, 2)) #array of pairs of (eigenvalues, freqency of certain eigenvalue)
"""
P = {}
wlist = []
for i, row in df2.iterrows():
	for col in df2:
		if col == "time":
			continue
		if row[col] not in wlist:
			wlist.append(row[col])
			if isnan(row[col]) == False:
				P[row[col]] = 1 #one instance of this new eigenval
				sumd += 1
		elif row[col] in wlist:
			P[row[col]] += 1
			sumd += 1
print "sumd = ", sumd
print "rows*N = ", rows*N
P[0.0] = 0 #this point skews graph limits
P[-0.0] = 0 #this point skews graph limits
#P = filter(lambda k: not isnan(k), P)
#print P
P_normalized = [x/(sumd) for x in P.values()]
f = open("eigendictionary.txt","w")
f.write(str(list(P.keys())) + "\n")
f.write(str(P.values()))
f.close()
#Pdf = pd.DataFrame(P)
#plt.figure()
#plt.plot(P["lambda[0]"], P, 'o')
plt.figure()
plt.plot(list(P.keys()),P.values(),'o') #points
#plt.figure()
#plt.plot(list(P.keys()), P.values()) #curve
plt.xlabel("Normal Mode Frequencies w");
plt.ylabel("Occurrence Number N(w)");
plt.title("Normal mode spectrum")
plt.savefig("N(w)_vs_w.png")
