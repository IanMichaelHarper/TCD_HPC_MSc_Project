import numpy as np

#print "hello from eigen.py" #show in c program that python script has run

L = np.genfromtxt("L.csv",delimiter=',',dtype=None)
L = np.array(L)
rows = L.shape[0]
cols = L.shape[1]
w,v = np.linalg.eig(L) #produce scientific notation output

g = open("eigenvalues.txt", 'w')
for i in range(rows):
	if i == rows-1:
		g.write(str(w[i]))
	else:
		g.write(str(w[i]) + ",");
g.close()

#f = open("eigenvectors.csv", 'w')
f = open("eigenvectors.txt", 'w')
for i in range(rows):
	for j in range(cols):
		if j == cols-1:
			f.write(str(v[i][j]))
		else:
			f.write(str(v[i][j]) + ",")
	f.write("\n")
f.write("EOF") #this line is now redundant
#print "eigen.py finished"
f.close()
