#include "header.h"

//20 particles confined in a shallow, one-dimensional
//harmonic well of stiffness k0 under conditions of underdamping
//viscous drag, low temperature, and strong, sinusoidal
//forcing [16,17] of a single particle (labeled with index 0)

int main(){
	//init random seed
	srand(time(NULL));
	
	//alloc memory	
	double * x = calloc(N, sizeof(double)); //start at origin
	double * xdot = calloc(N, sizeof(double)); //start at origin
	double * x_double_dot = calloc(N, sizeof(double)); //start at origin
	double * z = calloc(N, sizeof(double)); //start at origin
	double * f = calloc(N, sizeof(double)); //"normal mode" force
	int ** A = malloc(N*sizeof(int *));
	int * Adata = calloc(N*N,sizeof(int));
	int ** L = malloc(N*sizeof(int *));
	int * Ldata = calloc(N*N,sizeof(int));
	for (int i=0; i<N; i++){
		//x[i] = rand() % 10; //start at random locations
		x[i] = i - N/2; //start equally spaced in pot well
		A[i] = &Adata[i*N];
		L[i] = &Ldata[i*N];
	}
	setA(x,A);
	setL(A,L);
	write_graph(A); //write graph config to file for gephi visualization
	
	//init vars for dgeev()	
	double* a = calloc(N*N,sizeof(double)); //make new array for L matrix since a is overwritten by dgeev()	
	double * wr = calloc(N,sizeof(double));
	double * wi = calloc(N,sizeof(double));
	double * w = calloc(N,sizeof(double));
	double  * vl = calloc(N*N,sizeof(double));
	double * vr = calloc(N*N,sizeof(double));
	
	//print_particle_positions(x); //starting positions

	FILE * positions,*results,*all_evals,*g,*bf;
	init_files(positions,results,all_evals,g,bf);

	//init vars
	struct timeval start, end; //for timing
	long long time_taken;
	double P1,P2; //power dissipated
	int t = 0; //time 
	int dt = 1; //change in time between iterations
	int num_bonds, kL_sum;
	double Wtot = 0;
	
	//time loop
	int num_iters = 10000; //num steps in paper
	gettimeofday(&start,NULL);
	int temp_timelimit = 500;
	while (t < num_iters){

		if ((t % 500) == 0){ //t % temp_timelimit/10 doesn't work here?
			printf("t = %d\n", t);
	//		print_matrix(L);
	//		print_particle_positions(x);
		}
		P1 = P(x); 
		num_bonds = 0;
//		dt = log(1/drand48())/r(i,j,x,A); //Gillespie's Algo
		for (int i=0; i<N; i++){
			kL_sum = 0;
			for (int j=0; j<N; j++){
				if (j < i){ //don't double count 
					if (A[i][j] == 1)
						num_bonds++;
				}
			
//				if (t % 100 == 0)
				//if (r(i,j,x) != 0 && r(i,j,x) != 1.340642)
//					printf("r = %lf\n", r(i,j,x));
				kL_sum -= k*L[i][j]*x[j];
				//kL_sum -= k*r(i,j,x)*x[j];
			}
//			if (t % 500 == 0 && i % 5 == 0) printf("kl_sum = %d\n", kL_sum);
			if (i == 0)
				x_double_dot[i] = (kL_sum - b*xdot[i] + F*sin(omega_d * t) - k0*x[i])/m; 
			else 
				x_double_dot[i] = (kL_sum - b*xdot[i] - k0*x[i])/m;
			//x_double_dot[i] = (kL_sum - b*xdot[i] - k0*x[i])/m; //non-driven system
			
			//update time and particle positions and velocities for next iteration
			xdot[i] += x_double_dot[i] * dt; //dt here is the change in time between this iteration and nex iteration
			x[i] += xdot[i] * dt;
		}
//		printf("\n");
		P2 = P(x);

//		printf("H = %lf\n", H(x,xdot,A));
		g = fopen("energy.csv", "a");
		Wtot += W(t,xdot);
		fprintf(g, "%d,%lf,%lf,%lf\n", t, H(x,xdot,A), W(t,xdot), Wtot);
		fclose(g);	
		bf = fopen("bonds.csv", "a");
		fprintf(bf,"%d,%lf\n", t, (double)num_bonds/(double)N);
		fclose(bf);
//		printf("sizeof x = %lf\n", sizeof(x)/sizeof(x[0]));
		
		/*write particle positions to file for plotting
		positions = fopen("positions.csv", "a");
		fprintf(positions, "%d", t);
		for (int i=0; i<N; i++){
			fprintf(positions, ",%lf", x[i]);
			pos_sum += abs(x[i]);
		}
		fprintf(positions, ",%lf\n", pos_sum);
		fclose(positions);
		pos_sum = 0; //reset for next iteration
		*/

		//write values to use for power spectrum
		results = fopen("results.csv","a"); //might need "a" 
		fprintf(results,"%d,%lf,%lf\n", t, P1, P2);
		fclose(results);
		
		int checkL2a = false;
		if (checkL2a == true){
			if (t % 500 == 0){
				for (int i=0; i<N; i++){
					for (int j=0; j<N; j++){
						a[i*N + j] = L[i][j];
						printf("%lf ", a[i*N + j]);
					}
					printf("\n");
				}
			}
		}
		else{
			for (int i=0; i<N; i++){
				for (int j=0; j<N; j++)
					a[i*N + j] = L[i][j];
			}
		}
		
		lapack_int n=N, lda = N, ldvl = N, ldvr = N;
		lapack_int info = LAPACKE_dgeev(LAPACK_ROW_MAJOR,'N','V',n,a,lda,wr,wi,vl,ldvl,vr,ldvr); //get eigenvars
	
		//get normal mode displacements z
		for (int i=0; i<N; i++){
			f[i] = F*vr[0*N + i];
			z[i] = (f[i]*cexp(I*omega_d*t))/(k0*wr[i] - m*pow(omega_d,2) + I*b*omega_d); //k0 could be k here, not sure - possibly equivalent
		}
		positions = fopen("positions.csv", "a");
		fprintf(positions, "%d", t);
		double pos_sum = 0; //reset for next iteration
		for (int i=0; i<N; i++){
			fprintf(positions, ",%lf", x[i]);
			pos_sum += abs(x[i]);
		}
		fprintf(positions, ",%lf\n", pos_sum);
		fclose(positions);

		all_evals = fopen("all_evals.csv", "a");
		fprintf(all_evals, "%d", t);
		for (int i=0; i<N; i++){
			fprintf(all_evals, ",%lf",wr[i]);
		}
		fprintf(all_evals, ",%lf\n", mu(wr));
		fclose(all_evals);
  	//	fprintf(gnuplotPipe, "%d %lf\n", t, x[0]);
		setA(x,A);
		setL(A,L);
		t += dt;
		/*
 		if (t < 5){
			printf("Adjacency matrix after 1 iteration:\n"); 
			printA(A);
		}*/
	}

	//fprintf(gnuplotPipe, "e");
	//pclose(gnuplotPipe);
	gettimeofday(&end,NULL);
//	char pycode[] = "eigen.py";
//	exec_pycode(pycode);
	print_particle_positions(x);
	double div = 1000000;
	time_taken = (end.tv_sec - start.tv_sec)*100000L + (end.tv_usec - start.tv_usec);
	double time_in_usec = (double)time_taken;
	double time_taken_in_seconds = time_in_usec/div;
	printf("simulation took %lf seconds\n", time_taken_in_seconds);
	FILE * timeTaken = fopen("timeTaken.txt", "a"); //change w to a here for ensemble
	fprintf(timeTaken, "a single simulation of %d particles in serial took %lf seconds\n", N, time_taken_in_seconds);
	fclose(timeTaken);

	/*plot graph
	FILE * gnuplotPipe = popen ("gnuplot -persistent", "w");
	fprintf(gnuplotPipe, "plot '-' \n");
	for (i = 0; i < iterations; i++)
	{
  		fprintf(gnuplotPipe, "%d %d\n", i, tot_fit_arr[i]);
	}
	fprintf(gnuplotPipe, "e");
	pclose(gnuplotPipe);
	*/	
	free(x);
	free(xdot);
	free(x_double_dot);
	free(z);
	free(f);
	free(A[0]);//might not need this line after free(U[[0])
	free(A);
//	free(bond_rates);
	free(w);
	free(wr);
	free(wi);
	free(a);
	free(vl);
	free(vr);
	
	return 0;
}
