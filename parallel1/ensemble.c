#include <stdio.h>
#include <stdlib.h>

#define N 20

int main(){
	//eigenvalues file
	FILE * all_evals = fopen("all_evals.csv", "w");
	if (all_evals == NULL){
		printf("file is null, exiting");
		exit(1);
	}
	fprintf(all_evals, "time");
	for (int i=0; i<N; i++){
		fprintf(all_evals, ",lambda[%d]", i);
	}
	fprintf(all_evals, ",mu\n");
	fclose(all_evals);

	//run multiple simulations to get ensemble of trajectories
	int num_sims = 50;
	for (int k=0; k<num_sims; k++)
		system("./life");

	return 0;
}
