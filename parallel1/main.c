#include "header.h"

//20 particles confined in a shallow, one-dimensional
//harmonic well of stiffness k0 under conditions of underdamping
//viscous drag, low temperature, and strong, sinusoidal
//forcing [16,17] of a single particle (labeled with index 0)

int main(int argc, char * argv[]){
	//init random seed
	srand(time(NULL));

	int rank,size;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Status stat;	
	MPI_Request reqs[2]; //for pos array on rank 0 
	//alloc memory	
	/*if (rank == 0 || rank == size-1)
		double * x = calloc(N/size + 1, sizeof(double)); //start at origin //one ghost slot for either end
	else
		double * x = calloc(N/size + 2, sizeof(double)); //start at origin //two ghost rows for middle of array
	*/
	double * x = calloc(N, sizeof(double)); //start at origin //two ghost rows for middle of array
	double * xdot = calloc(N, sizeof(double)); //start at origin
	double * x_double_dot = calloc(N, sizeof(double)); //start at origin
	double * z = calloc(N, sizeof(double)); //start at origin
	double * f = calloc(N, sizeof(double)); //"normal mode" force
	double * lambda = calloc(N,sizeof(double)); //unneccessary in terms of storage
	double ** U = malloc(N*sizeof(double *)); //also unnecessary
	double * Udata = calloc(N*N,sizeof(double));
	int ** A = malloc(N*sizeof(double *));
	int * Adata = calloc(N*N,sizeof(double));
	int ** L = malloc(N*sizeof(double *));
	int * Ldata = calloc(N*N,sizeof(double));
	double *a,*wr,*wi,*w,*vl,*vr,*pos;
	if (rank == 0){	
		a = calloc(N*N,sizeof(double)); //make new array for L matrix since a is overwritten by dgeev()	
		wr = calloc(N,sizeof(double));//real part of eigenvalues
		wi = calloc(N,sizeof(double)); //imaginary part of eigenvalues
		w = calloc(N,sizeof(double)); //eigenvalues
		vl = calloc(N*N,sizeof(double)); //left eigenvectors
		vr = calloc(N*N,sizeof(double)); //right eigenvectors
		pos = calloc(N, sizeof(double)); //positions of particles stored for writing to file
	}
	int rough_std_dev = 6; //particles will be in [-6,6] 68% of the time
	for (int i=0; i<N; i++){
		//x[i] = rand() % (10+rank*2); //start at random locations - rank term to ensure all particiles have unique starting points
		//x[i] = i-10; //start equally spaced in pot well
		x[i] = 'NULL'; //signifies data on other processor
		xdot[i] = 'NULL'; //signifies data on other processor
		x_double_dot[i] = 'NULL'; //signifies data on other processor
		//init positions evenly spaced out in potential well, with spatially decomposed proc grid
		if (i-10 <=-rough_std_dev){
			if (rank == 0){
				x[i] = i-10;
				pos[i] = i-10;
			}
		}
		else if (i-10 > -rough_std_dev && i-10 <= 0){
			if (rank == 1){
				x[i] = i-10;
				MPI_Send(&x[i],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD); 
			}
			else if (rank == 0)
				MPI_Recv(&pos[i],1,MPI_DOUBLE,1,0,MPI_COMM_WORLD,&stat);
		}
		else if (i-10 > 0 && i-10 < rough_std_dev){
			if (rank == 2){
				x[i] = i-10;
				MPI_Send(&x[i],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD); 
			}
			else if (rank == 0)
				MPI_Recv(&pos[i],1,MPI_DOUBLE,2,0,MPI_COMM_WORLD,&stat);
		}
		else{
			if (rank == 3){
				x[i] = i-10;
				MPI_Send(&x[i],1,MPI_DOUBLE,0,0,MPI_COMM_WORLD); 
			}
			else if (rank == 0)
				MPI_Recv(&pos[i],1,MPI_DOUBLE,3,0,MPI_COMM_WORLD,&stat);
		}
		U[i] = &Udata[i*N];
		A[i] = &Adata[i*N];
		L[i] = &Ldata[i*N];
		MPI_Barrier(MPI_COMM_WORLD);
	}
	for (int proc = 0; proc < size; proc++){
		if (proc == rank)
			print_particle_positions(x); //starting positions
		MPI_Barrier(MPI_COMM_WORLD);
	}
	if (rank == 0){
		printf(" ------------- pos ---------------\n");
		print_particle_positions(pos); //starting positions
	}
	if (rank == 0){ //serialize for now
		setA(pos,A);
		setL(A,L);
	}
/*
 	printf("Adjacency Matrix A:\n");
	print_matrix(A);
	printf("Laplacian Matrix L:\n");
	print_matrix(L);
	write_graph(A); //write graph config to file for gephi visualization
*/
	//printf("r_avg = %lf\n", exp(-beta*k*pow(delta_x(0,1,x),2))*I0(beta*k*pow(delta_x(0,1,x),2)/2));
//	double * bond_rates = calloc(N*N,sizeof(double));

	FILE * positions,*results,*all_evals,*g,*bf;
	if (rank == 0){
		init_files(positions,results,all_evals,g,bf);
	}
	MPI_Barrier(MPI_COMM_WORLD);
	
	//init vars
	struct timeval start, end; //for timing
	long long time_taken;
	double P1,P2; //power dissipated
	int t = 0; //time 
	int dt = 1; //change in time between iterations
	int num_bonds, kL_sum, all_procs_kL_sum;
	double Wtot = 0;
	double temp;	
	int flag = false;
	int src,dest;
	//time loop
	double avg_position = 0;
	double std_dev = 0;
	double pos_sum = 0;
	int num_iters = 10000; //num steps in paper
	gettimeofday(&start,NULL);
	int temp_timelimit = 500;
	while (t < num_iters){
		if ((t % 50) == 0){ //t % temp_timelimit/10 doesn't work here?
			if (rank == 0){
				printf("t = %d\n", t);
			//	printf("Adjacency Matrix A:\n");
			//	print_matrix(A);
			//	printf("Laplacian Matrix L:\n");
			//	print_matrix(L);
			}
			//print_particle_positions(x);
		}
		//if (t % 100 == 0) print_particle_positions(x);
//		get_bond_rates(x,bond_rates);
		num_bonds = 0;
		P1 = P(x); 
		avg_position = 0;
//		dt = log(1/drand48())/r(i,j,x,A); //Gillespie's Algo
		for (int i=0; i<N; i++){
			kL_sum = 0;
			for (int j=0; j<N; j++){
				if (j < i){ //don't double count 
					if (A[i][j] == 1)
						num_bonds++;
				}
			
//				if (t % 100 == 0)
				//if (r(i,j,x) != 0 && r(i,j,x) != 1.340642)
//					printf("r = %lf\n", r(i,j,x));
				for (int proc = 0; proc < size; proc++){
					if (rank == proc){
						if (L[i][j] != not_here && x[j] != 'NULL'){
							if (t == 0 && i == 0 && j == 0) printf("calculating kl_sum on rank %d\n", rank);
							kL_sum -= k*L[i][j]*x[j];
						}
						//safeguards:
						else if (L[i][j] != not_here && x[j] == 'NULL'){
							printf("L[%d][%d] is here but x[%d] isnt, find out whats happening. Stopping program...\n",i,j,j);
							print_matrix(L);
							print_particle_positions(x);
							exit(1);
						}
						else if (L[i][j] == not_here && x[j] != 'NULL'){
							printf("L[%d][%d] = %d isnt here but x[%d] = %lf is, find out whats happening. Stopping program...\n",i,j,L[i][j],j,x[j]);
							print_matrix(L);
							print_particle_positions(x);
							exit(1);
						}
					}
				}
				/*else{
					dest = rank;
					for (int proc=0; proc<size; proc++){
						if (rank == proc){
							if (x[j] != 'NULL'){
								MPI_Send(&x[j], 1, MPI_DOUBLE, dest, 0, MPI_COMM_WORLD, &stat);
							}
						}
					}
				}*/ 
			}
			if (t % 50 == 0 && i % 5 == 0) printf("kl_sum = %d on rank %d\n", kL_sum, rank);
			for (int root = 0; root < size; root++)
				MPI_Reduce(&kL_sum, &all_procs_kL_sum, 1, MPI_INT, MPI_SUM, root, MPI_COMM_WORLD);
/*			if (kL_sum != 0){
				printf("kl sum = %d on rank %d\n", kL_sum, rank);
				printf("all procs kl sum = %d on rank %d\n", all_procs_kL_sum, rank);
			}
*/		
			if (t % 50 == 0 && i % 5 == 0) printf("all_proc_kl_sum = %d on rank %d\n", all_procs_kL_sum, rank);
			//Equation of Motion calculation: a = F/m
			if (x[i] != 'NULL'){ //could try put safeguard here to give error if no or more than 1 process enters this if statement
				if (i == 0) //driving force only applied to particle 0
					x_double_dot[i] = (all_procs_kL_sum - b*xdot[i] + F*sin(omega_d * t) - k0*x[i])/m;
				else 
					x_double_dot[i] = (all_procs_kL_sum - b*xdot[i] - k0*x[i])/m;
				//update time and particle positions and velocities for next iteration
				xdot[i] += x_double_dot[i] * dt; //dt here is the change in time between this iteration and nex iteration
				if (xdot[i] == not_here){ //for whatever reason
					printf("\n XDOT[I] == NOT_HERE, X[I] WILL BE OUTSIDE RANGE IN LAST ELSE STATEMENT IN EOM CALCULATION \n");
					exit(1);
				}
				x[i] += xdot[i] * dt;
				avg_position += x[i];
				
				//change x array to accommodate spatial decomposition
				//new positions are taken from non-null processors and given to processors allocated to that region in pot well
				if (i-10 <=-6){
//					printf("rank %d has x[i] <= -6\n", rank);
					temp = x[i]; //get value from all proc with non-null value
					x[i] = not_here; //replace this processors value of x[i] with not_here
					if (rank == 0) 
						x[i] = temp; //give to proc 0
				}
				else if (i-10 > -6 && i-10 <= 0){
//					printf("rank %d has x[i] in (-6,0]\n", rank);
					temp = x[i]; //get value from all proc with non-null value
					x[i] = not_here; //replace this processors value of x[i] with not_here
					if (rank == 1)
						x[i] = temp; //give to proc 1
				}
				else if (i-10 > 0 && i-10 < 6){
//					printf("rank %d has x[i] in (0,6)\n", rank);
					temp = x[i]; //get value from all proc with non-null value
					x[i] = not_here; //replace this processors value of x[i] with not_here
					if (rank == 2)
						x[i] = temp; //give to proc2 
				}
				else{ //this might cause problems since not_here gets printed as a very large number
//					printf("rank %d has x[i] >= 6, value is %lf\n", rank, x[i]);
					temp = x[i]; //get value from all proc with non-null value
					x[i] = not_here; //replace this processors value of x[i] with not_here
					if (rank == 3)
						x[i] = temp; //give to proc 3 
				}
			}
			MPI_Barrier(MPI_COMM_WORLD);
			//update pos array on rank 0
			flag = false;
			if (rank == 0)
				pos[i] = x[i];
			else{
				flag = true; //indicates rank 0 must recv
				MPI_Isend(&x[i], 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &reqs[0]); //more comm - bottleneck **should change to Isend
			}
			MPI_Barrier(MPI_COMM_WORLD); //here to make sure rank 0 doesnt check value of flag before its set to true - prevents race cond
			if (flag == true && rank == 0)
				MPI_Irecv(&pos[i], 1, MPI_DOUBLE,MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &reqs[1]);
			MPI_Barrier(MPI_COMM_WORLD);
			//x_double_dot[i] = (kL_sum - b*xdot[i] - k0*x[i])/m; //non-driven system
		}
		if (t == 2){
			if (rank == 0)
				print_particle_positions(pos);
			MPI_Barrier(MPI_COMM_WORLD);
			print_particle_positions(x);
			exit(1);
		}
//		printf("rank %d got EOM calc\n", rank);
		MPI_Barrier(MPI_COMM_WORLD);
//		printf("rank %d got before sendrecv\n", rank);
		avg_position = avg_position/N;
		std_dev = 0;
		for (int i=0; i<N; i++)
			std_dev += pow(x[i] - avg_position, 2);
		std_dev = sqrt(std_dev/N-1);
		P2 = P(x);
/*		printf("rank %d got after Power func\n", rank);
		for (int proc = 0; proc < size; proc++){
			if (rank == proc){
				printf("rank %d matrix:\n", rank);
				print_matrix(L);
			}
			MPI_Barrier(MPI_COMM_WORLD);
		}
*/		//pre-eigen assignment	
		for (int i=0; i<N; i++){
			for (int j=0; j<N; j++){ //L is symmetric so can cut this loop in half
				flag = true; //assume desired L[i][j] is not on rank 0
				if (L[i][j] != not_here){
					if (rank == 0){ //since we are sending to rank 0 no need to send info here
						a[i*N + j] = L[i][j];
						flag = false; //flag to prevent rank 0 receiving
					}
					else{
					//	printf("rank %d is about to send L[%d][%d] = %d\n", rank, i, j, L[i][j]);
						MPI_Send(&L[i][j],1,MPI_INT,0,0,MPI_COMM_WORLD); //send L value to rank 0
					}
				}
				//printf("rannk %d is waiting at barrier\n");
				if (rank == 0 && flag == true)
					MPI_Recv(&a[i*N + j],1,MPI_DOUBLE,MPI_ANY_SOURCE,0,MPI_COMM_WORLD,&stat); //rank 0 receives data - would like more precise way of knowing source proc instead of MPIANY_SOURCE
				MPI_Barrier(MPI_COMM_WORLD);
			//	printf("rannk %d is waiting at barrier\n");
			//	printf("rannk %d is past barrier\n");
			}
		}
		//printf("rank %d got before eigensolve\n", rank);
		if (rank == 0){
			if (t % 50 == 0){
				printf("--------------------a matrix --------------------------\n");
				for (int i=0; i<N; i++){
					for (int j=0; j<N; j++){
						printf("%lf ", a[i*N + j]);
					}
					printf("\n");
				}
				printf("\n");
			}
			//eigencalulation	
			lapack_int n=N, lda = N, ldvl = N, ldvr = N;
			lapack_int info = LAPACKE_dgeev(LAPACK_ROW_MAJOR,'N','V',n,a,lda,wr,wi,vl,ldvl,vr,ldvr); //get eigenvars
			//get normal mode displacements z
			for (int i=0; i<N; i++){
				f[i] = F*U[0][i];
				lambda[i] = wr[i];
				z[i] = (f[i]*cexp(I*omega_d*t))/(k0*lambda[i] - m*pow(omega_d,2) + I*b*omega_d); //k0 could be k here, not sure - possibly equivalent
			}

			//write to files
	//		printf("H = %lf\n", H(x,xdot,A));
			g = fopen("energy.csv", "a");
			Wtot += W(t,xdot);
			fprintf(g, "%d,%lf,%lf,%lf\n", t, H(x,xdot,A), W(t,xdot), Wtot);
			fclose(g);	
			bf = fopen("bonds.csv", "a");
			fprintf(bf,"%d,%lf\n", t, (double)num_bonds/(double)N);
			fclose(bf);
	//		printf("sizeof x = %lf\n", sizeof(x)/sizeof(x[0]));
			
			//write particle positions to file for plotting
			positions = fopen("positions.csv", "a");
			fprintf(positions, "%d", t);
			for (int i=0; i<N; i++){
				fprintf(positions, ",%lf", pos[i]);
				pos_sum += abs(pos[i]);
			}
			fprintf(positions, ",%lf", pos_sum);
			fprintf(positions, ",%lf", avg_position);
			fprintf(positions, ",%lf\n", std_dev);
			fclose(positions);
			pos_sum = 0; //reset for next iteration

			//write values to use for power spectrum
			results = fopen("results.csv","a"); //might need "a" 
			fprintf(results,"%d,%lf,%lf\n", t, P1, P2);
			fclose(results);
		
			all_evals = fopen("all_evals.csv", "a");
			fprintf(all_evals, "%d", t);
			for (int i=0; i<N; i++){
				fprintf(all_evals, ",%lf",wr[i]);
			}
			fprintf(all_evals, ",%lf\n", mu(wr));
			fclose(all_evals);
		}
		MPI_Barrier(MPI_COMM_WORLD);
		if (t % 50 == 0)
			printf("rank %d got to code after write files\n", rank);
		setA(x,A); //need to pass in pos instead of x and re-write function since it is not accounting for global bonds right now
		setL(A,L);
		t += dt;
	}
	printf("rank %d waiting at barrier after while loop\n", rank);
	MPI_Barrier(MPI_COMM_WORLD);
	gettimeofday(&end,NULL);
	print_particle_positions(x);
	double div = 1000000;
	time_taken = (end.tv_sec - start.tv_sec)*100000L + (end.tv_usec - start.tv_usec);
	double time_in_usec = (double)time_taken;
	double time_taken_in_seconds = time_in_usec/div;
	printf("simulation took %lf seconds\n", time_taken_in_seconds);

	free(x);
	free(xdot);
	free(x_double_dot);
	free(U[0]); 
	free(U); //might not need this line after free(U[[0])	
	free(lambda);
	free(z);
	free(f);
	free(A[0]);
	free(A);
//	free(bond_rates);
	if (rank == 0){
		free(w);
		free(wr);
		free(wi);
		free(a);
		free(vl);
		free(vr);
		free(pos);
	}
	printf("rank %d got between frees and MPI_Finalize()\n", rank);

	MPI_Finalize();	
	return 0;
}
