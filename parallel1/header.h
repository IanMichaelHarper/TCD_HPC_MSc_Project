#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <complex.h>
#include <mkl.h>
#include <mpi.h>

//constants
#define N 10//number of particles
#define k0 0.1 //need to find this number - harmonic well stiffness
#define b 0.01 //drag
#define T 1.8116e+19 //temperature in Kelvin
#define F 10 //max sinusoidal force
#define m 1 //mass of each particle. Could make this variable maybe to get more interesting results
#define k 1 //bond/spring constant
#define omega_d 1.2 //wd = 1.2 for normal mode spectrum graph
#define rO 1 //bond rate - needs to be much less than omega_d
#define epsilon 0.0001 //baseline bonding energy
#define boltzmann 1.38e-23
#define beta 4000

#define not_here 'NULL'
#define getName(var)  #var

#define true 1
#define false 0

//functions
double delta_x(int i, int j, double * x);
double Ea(int i, int j, double * x, int ** Adj);
double B(int i, int j, double * x);
void setA(double * x, int ** Adj);
void setL(int ** A, int ** L);
//int L(int i, int j, double * x, double ** Adj);
double E(int i, int j, double * x, int ** Adj);
double r(int i, int j, double * x, int ** Adj);
double H(double * x, double * xdot, int ** A);
double W(int t, double * xdot);
double I0(double y);
double mu(double * lambda);
void print_matrix(int ** A);
void print_particle_positions(double * x);
double P(double * x);
//void eigen(gsl_matrix * U, int ** A);
void init_files(FILE * positions, FILE * results, FILE * all_evals, FILE * g, FILE * bf);
void writefiles(FILE * positions, FILE * results, FILE * all_evals, FILE * g, FILE * bf);
void writeL2file(char * filename, int ** A);
void write_graph(int ** A);
void read_eigenvectors(char * filename, double ** U);
void read_eigenvalues(char * filename, double * lambda);
