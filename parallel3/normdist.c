#include <stdio.h>
#include <gsl/gsl_cdf.h>
#include <limits.h>

int main (void){
	double P, Q;
	double x = 2.0;
	P = gsl_cdf_ugaussian_P (x);
	printf ("prob(x < %lf) = %lf\n", x, P);
	printf ("Pinv(%lf) = %lf\n", P, x);
	x = gsl_cdf_ugaussian_Qinv (Q);
	printf ("prob(x > %lf) = %lf\n", x, Q);
	printf ("Qinv(%lf) = %lf\n", Q, x);

	printf("---------new-----------\n");
	
	int size = 4;
	double hundyp = 1;
	double std = 6;
	double wantP = hundyp/size;
	printf("wantP = %lf\n", wantP);	
	
	//double calcP = gsl_cdf_ugaussian_P(x);
	//printf("calcP = %lf\n", calcP);	

	printf("so our spatial divison should be ");
	for (int i=1; i<size; i++){
		x = gsl_cdf_gaussian_Pinv (wantP*i, std);
		printf("x = %lf ", x);	
	}
	printf("\n");

	double inf = pow(2, 1000000);
	printf("inf = %g\n", inf);
	if (-10000 > -inf) printf(" inf gret\n");
	return 0;
}   
