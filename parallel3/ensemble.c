#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define N 20 

int main(){
	//eigenvalues file
	FILE * all_evals = fopen("all_evals.csv", "w");
	if (all_evals == NULL){
		printf("file is null, exiting");
		exit(1);
	}
	fprintf(all_evals, "time");
	for (int i=0; i<N; i++){
		fprintf(all_evals, ",lambda[%d]", i);
	}
	fprintf(all_evals, ",mu\n");
	fclose(all_evals);

	//Power at adjacent steps file
	FILE * results = fopen("results.csv", "w");
	if ( results == NULL){
		printf("file is null, exiting");
		exit(1);
	}
	fprintf(results,"t,P1,P2\n");
	fclose(results);
	
	//runtimes
	FILE * timeTaken = fopen("timeTaken.txt", "w"); //change w to a here for ensemble
	fprintf(timeTaken, "Here are the runtimes for a system of %d particles:\n", N);
	fclose(timeTaken);
	
	//run multiple simulations to get ensemble of trajectories
	int num_sims = 50;
	struct timeval start, end;
	long long time_taken;
	gettimeofday(&start, NULL);
	for (int k=0; k<num_sims; k++){ //this loop can be parallelized simply by using OpenMP directives
		printf("simulation #%d\n", k);
		system("mpirun -np 3 ./life");
	}
	gettimeofday(&end, NULL);
	double div = 1000000;
	time_taken = (end.tv_sec - start.tv_sec)*100000L + (end.tv_usec - start.tv_usec);
	double time_in_usec = (double)time_taken;
	double time_taken_in_seconds = time_in_usec/div;
	printf("ensemble took %lf seconds\n", time_taken_in_seconds);
	
	timeTaken = fopen("timeTaken.txt", "a"); //change w to a here for ensemble
	fprintf(timeTaken, "an ensemble of %d simulations took %lf seconds\n", num_sims, time_taken_in_seconds);
	fclose(timeTaken);

	return 0;
}
