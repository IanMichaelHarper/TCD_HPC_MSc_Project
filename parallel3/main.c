#include "header.h"

//20 particles confined in a shallow, one-dimensional
//harmonic well of stiffness k0 under conditions of underdamping
//viscous drag, low temperature, and strong, sinusoidal
//forcing of a single particle (labeled with index 0)

int main(int argc, char * argv[]){
	//init random seed
	srand(time(NULL));

	//init MPI
	int rank,size;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Status stat; //for reg Recvs	
	
	//alloc memory	
	double * x = calloc(N, sizeof(double)); //start at origin //two ghost rows for middle of array
	double * xdot = calloc(N, sizeof(double)); //start at origin
	double * x_double_dot = calloc(N, sizeof(double)); //start at origin
	int ** A = malloc(N*sizeof(int *)); //Adjacency matrix
	int * Adata = calloc(N*N,sizeof(int ));
	int ** L = malloc(N*sizeof(int *)); //Graph laplacian matrix
	int * Ldata = calloc(N*N,sizeof(int));
	int ** flags = malloc(size * sizeof(int *)); //truth table for sending and receiving, initialised to false
	int * flagdata = calloc(size*size, sizeof(int)); //truth table for sending and receiving, initialised to false
	double *z,*f,*a,*wr,*wi,*w,*vl,*vr,*pos; //eigenvars and pos
	if (rank == 0){
		z = calloc(N, sizeof(double)); //start at origin
		f = calloc(N, sizeof(double)); //"normal mode" force
		a = calloc(N*N,sizeof(double)); //make new array for L matrix since a is overwritten by dgeev()	
		wr = calloc(N,sizeof(double));//real part of eigenvalues
		wi = calloc(N,sizeof(double)); //imaginary part of eigenvalues
		w = calloc(N,sizeof(double)); //eigenvalues
		vl = calloc(N*N,sizeof(double)); //left eigenvectors
		vr = calloc(N*N,sizeof(double)); //right eigenvectors
		pos = calloc(N, sizeof(double)); //positions of particles stored for writing to file
	}
	double rough_std_dev = 6; //scale standard deviation with particle number
	//double rough_std_dev = 6*(N/20.0); //scale standard deviation with particle number
	double * regions = calloc(size, sizeof(double)); //the x values that mark processor boundaries
	regions[0] = -pow(2,100000); //represents -infinity
	double hundyp = 1; //100%
	double wantP = hundyp/size; //we want a probability distribution that is equally spaced throughout our decomposition
	printf("so our spatial divison should be ");
	for (int p=1; p<size; p++){
		regions[p] = gsl_cdf_gaussian_Pinv (wantP*p, rough_std_dev);
		printf("x = %lf ", regions[p]);	
	}
//	regions[size-1]?
	printf("x = %lf ", -regions[0]); //+infinity	
	printf("\n");
	for (int i=0; i<N; i++){ //loop through particles to initialise them
		A[i] = &Adata[i*N];
		L[i] = &Ldata[i*N];
		//x[i] = rand() % (10+rank*2); //start at random locations - rank term to ensure all particiles have unique starting points
		x[i] = not_here; //signifies data on other processor
		if (rank == 0) pos[i] = i - N/2; //init global position array on rank 0
		//init positions evenly spaced out in potential well, with spatially decomposed proc grid
		for (int p=0; p<size; p++){ //loop through processors
			if (p == size-1){
				if (i - N/2 > regions[p]){
					if (rank == p)
						x[i] = i - N/2;
				}
			}
			else{
				if (i - N/2 > regions[p]){ //if the position (i - N/2) is greater than lower region limit
					if (i - N/2 <= regions[p+1]){ //and its less than the upper limit
						if (rank == p){ //than its in this processors regions
							x[i] = i - N/2; //so assign it a valid value on this processor
							break;
						}
					}
				}

			}
		}
	}
	parallel_particle_print(x,pos,size,rank);
	for (int proc = 0; proc < size; proc++)
		flags[proc] = &flagdata[proc*size];
#if 0 
	for (int proc = 0; proc < size; proc++){
		if (proc == rank)
			print_particle_positions(x); //starting positions
		MPI_Barrier(MPI_COMM_WORLD);
		usleep(5);
	}
	if (rank == 0){
		printf(" ------------- pos ---------------\n");
		print_particle_positions(pos); //starting positions
	}
#endif
	if (rank == 0){ //serialize for now
		setA(pos,A);
		setL(A,L);
	}
	MPI_Bcast(&A[0][0], N*N, MPI_INT, 0, MPI_COMM_WORLD);
	MPI_Bcast(&L[0][0], N*N, MPI_INT, 0, MPI_COMM_WORLD);
#if 0
 	printf("Adjacency Matrix A:\n");
	print_matrix(A);
	printf("Laplacian Matrix L:\n");
	print_matrix(L);
	write_graph(A); //write graph config to file for gephi visualization
#endif
	//init files for results
	FILE * positions,*results,*all_evals,*g,*bf;
	if (rank == 0){
		init_files(positions,results,all_evals,g,bf);
	}
	
	//init vars
	struct timeval start, end; //for timing
	struct timeval begin, finish; //for timing every 500 iterations
	long long time_taken;
	double div = 1000000;
	double time_in_usec;
	double time_taken_in_seconds;
	double P1,P2; //power dissipated
	int t = 0; //time 
	int dt = 1; //change in time between iterations
	int num_bonds; 
	double kL_sum, all_procs_kL_sum;
	double Wtot = 0;
	int flag = false;
	double avg_position = 0;
	double std_dev = 0;
	double pos_sum = 0;
	
	//time loop
	int num_iters = 10000; //num steps in paper
	gettimeofday(&start,NULL);
	gettimeofday(&begin,NULL);
	int temp_timelimit = 500;
	while (t < num_iters){
		if ((t % 500) == 0){ //t % temp_timelimit/10 doesn't work here?
			if (rank == 0){
				printf("\nt = %d\n", t);
				gettimeofday(&finish,NULL);
				time_taken = (finish.tv_sec - begin.tv_sec)*100000L + (finish.tv_usec - begin.tv_usec);
				time_in_usec = (double)time_taken;
				time_taken_in_seconds = time_in_usec/div;
				time_taken = (end.tv_sec - start.tv_sec)*100000L + (end.tv_usec - start.tv_usec);
				printf("simulation took %lf seconds\n", time_taken_in_seconds);
				gettimeofday(&begin,NULL);
			}
//			MPI_Barrier(MPI_COMM_WORLD);
//			parallel_particle_print(x,pos,size,rank); //if we uncomment this line, we must insert an MPI_Barrier() beforehand to ensure all the data for the pos array is received before continuing onto the next iteration. This will hinder performs for the benefit of a readout
		}
		num_bonds = 0;
		P1 = P(x); 
		avg_position = 0;
//		dt = log(1/drand48())/r(i,j,x,A); //Gillespie's Algo
		for (int i=0; i<N; i++){
			kL_sum = 0;
			for (int j=0; j<N; j++){
				if (j < i){ //don't double count 
					if (A[i][j] == 1)
						num_bonds++;
				}
				if (x[j] != not_here){ //x array changes after this statement - prob buffer overflow - old comment, fixed.
#if 0			
					if (abs(x[j]) > 10000 || abs(L[i][j]) > 10000){ 
						printf("current rank = %d\n", rank);
						//print_particle_positions(x); 
						printf("x[%d] = %lf on rank %d\nL[%d][%d] = %d at t = %d on rank %d\n",j,x[j],rank,i,j,L[i][j],t, rank); 
						printf("rank %d is exiting on line %d\n", rank, __LINE__);
						//exit(1); 
					}
#endif
					kL_sum -= k*L[i][j]*x[j];
				}
			}
			MPI_Allreduce(&kL_sum, &all_procs_kL_sum, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
		
			//reset flags matrix for this iteration 
			for (int l=0; l<size; l++){ //i taken
				for (int j=0; j<size; j++)
					flags[l][j] = false; //assume we don't receive new x coords after update
			}
			flag = true; //assume this rank receives	
			//Equation of Motion calculation: a = F/m
			if (x[i] != not_here){ //could try put safeguard here to give error if more than 1 process enters this if statement
				flag = false; //only one rank enters here, so this rank don't receive flags matrix
//				if (x[i] > 10000) printf("x[%d] = %lf on rank %d at t = %d\n",i,x[i],rank,t);
				if (i < N/20.0) //driving force only applied to 1/20th of the number of particles
					x_double_dot[i] = (all_procs_kL_sum - b*xdot[i] + F*sin(omega_d * t) - k0*x[i])/m;
				else 
					x_double_dot[i] = (all_procs_kL_sum - b*xdot[i] - k0*x[i])/m;
		
				//update time and particle positions and velocities for next iteration
				xdot[i] += x_double_dot[i] * dt; //dt here is the change in time between this iteration and nex iteration
				if (xdot[i] == not_here){ //for whatever reason
					printf("\n XDOT[I] == NOT_HERE, X[I] WILL BE OUTSIDE RANGE IN LAST ELSE STATEMENT IN EOM CALCULATION \n");
					exit(1);
				}
				x[i] += xdot[i] * dt;
				avg_position += x[i];
		
				//change x array to accommodate spatial decomposition
				//new positions are taken from non-null processors and given to processors allocated to that region in pot well
				for (int p=0; p<size; p++){ //loop through processors
					if (p == size-1){
						if (x[i] > regions[p]){
							if (rank == p)
								flag = false;
							else
								flags[rank][p] = true; //processor p receive from processor "rank"
						}
					}
					else{
						if (x[i] > regions[p]){ //if the position (i - N/2) is greater than lower region limit
							if (x[i] <= regions[p+1]){ //and its less than the upper limit
								if (rank == p) //than its in this processors regions
									flag = false;//rank p don't receive since this x[i] val belongs here since this spatial range is rank 0's spatial range
								else{
									flags[rank][p] = true; //processor 0 receive from processor "rank"
									break;
								}
							}
						}

					}
				}
				//putting these sends outside if (rank in region) statements creates more comm but need something to tell other procs that they don't need to receive anyway because this code is isolated to 1 processor. Could rewrite in more efficient way potentially
				for (int proc = 0; proc < size; proc++){ //send flags to all procs
					if (proc != rank) //send to all procs other than this one
						MPI_Send(flagdata, size*size, MPI_INT, proc, 0, MPI_COMM_WORLD); //normal send okay here cause other procs are waiting to recv already
				} 
			}
			if (flag == true) //ranks that didnt send, receive
				MPI_Recv(flagdata, size*size, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD ,&stat); //double check if any_source works here 
			for (int a = 0; a < size; a++){
				for (int c = 0; c < size; c++){
					if (a == c) continue; //we dont want a proc to send to itself, no need for comm in this case - assume false
					if (flags[a][c] == true){ //there might only be one true in the matrix at each iteration? could put break statement if so to save time - confirmed.
						if (rank == a){
							MPI_Send(&x[i], 1, MPI_DOUBLE, c, 3, MPI_COMM_WORLD);
							MPI_Send(&xdot[i], 1, MPI_DOUBLE, c, 4, MPI_COMM_WORLD); //also need to send xdot since its used in EOM calculation - if we don't send and x[i] != not_here then xdot could be not_here and we will get the wrong result
			//				x[i] = not_here; //race cond here? 
						}
						else if (rank == c){
							MPI_Recv(&x[i], 1, MPI_DOUBLE, a, 3, MPI_COMM_WORLD, &stat);
							MPI_Recv(&xdot[i], 1, MPI_DOUBLE, a, 4, MPI_COMM_WORLD, &stat);
						}
						MPI_Barrier(MPI_COMM_WORLD);
						if (rank == a){
							x[i] = not_here; //here to prevent race cond 
							xdot[i] = not_here; //here to prevent race cond 
						}
						break; //no need to keep checking other values since we found our one true value
					}
				}
			}
			//update  pos array on rank 0
			flag = true;
			if (x[i] != not_here){ //if x[i] is on this rank
				if (rank == 0){
					pos[i] = x[i]; //get x val for global
					flag = false; //indicates we don't have to receive
				}
				else
					MPI_Send(&x[i], 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD); //send x val to pos
			}
			if (rank == 0 && flag == true) MPI_Recv(&pos[i], 1, MPI_DOUBLE, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &stat);
			//x_double_dot[i] = (kL_sum - b*xdot[i] - k0*x[i])/m; //non-driven system
		} //end of i'th iteration
		
		//get preliminary stats for spatial decomposition
		avg_position = avg_position/N;
		std_dev = 0;
		for (int i=0; i<N; i++)
			std_dev += pow(x[i] - avg_position, 2);
		std_dev = sqrt(std_dev/N-1);
		P2 = P(x);
		
		//pre-eigen assignment
		if (rank == 0){
			for (int i=0; i<N; i++){
				for (int j=0; j<N; j++){ //L is symmetric so can cut this loop in half
					a[i*N + j] = L[i][j]; //assign L values to a for eigensolve
				}
			}
#if 0 
			if (t % 500 == 0){
				printf("--------------------a matrix --------------------------\n");
				for (int i=0; i<N; i++){
					for (int j=0; j<N; j++){
						printf("%lf ", a[i*N + j]);
					}
					printf("\n");
				}
				printf("\n");
			}
#endif
			//eigencalulation	
			lapack_int n=N, lda = N, ldvl = N, ldvr = N;
			lapack_int info = LAPACKE_dgeev(LAPACK_ROW_MAJOR,'N','V',n,a,lda,wr,wi,vl,ldvl,vr,ldvr); //get eigenvars
			
			//get normal mode displacements z
			for (int i=0; i<N; i++){
				f[i] = F*vr[0*N + i];
				z[i] = (f[i]*cexp(I*omega_d*t))/(k0*wr[i] - m*pow(omega_d,2) + I*b*omega_d); //k0 could be k here, not sure - possibly equivalent
			}

			//write to files
			g = fopen("energy.csv", "a");
			Wtot += W(t,xdot);
			fprintf(g, "%d,%lf,%lf,%lf\n", t, H(x,xdot,A), W(t,xdot), Wtot);
			fclose(g);	
			bf = fopen("bonds.csv", "a");
			fprintf(bf,"%d,%lf\n", t, (double)num_bonds/(double)N);
			fclose(bf);
			
			//write particle positions to file for plotting
			positions = fopen("positions.csv", "a");
			fprintf(positions, "%d", t);
			for (int i=0; i<N; i++){
				fprintf(positions, ",%lf", pos[i]);
				pos_sum += abs(pos[i]);
			}
			fprintf(positions, ",%lf", pos_sum);
			fprintf(positions, ",%lf", avg_position);
			fprintf(positions, ",%lf\n", std_dev);
			fclose(positions);
			pos_sum = 0; //reset for next iteration

			//write values to use for power spectrum
			results = fopen("results.csv","a"); //might need "a" 
			fprintf(results,"%d,%lf,%lf\n", t, P1, P2);
			fclose(results);
		
			all_evals = fopen("all_evals.csv", "a");
			fprintf(all_evals, "%d", t);
			for (int i=0; i<N; i++){
				fprintf(all_evals, ",%lf",wr[i]);
			}
			fprintf(all_evals, ",%lf\n", mu(wr));
			fclose(all_evals);
			
			setA(pos,A);
			setL(A,L);
		}
		
		//send A and L to all other procs - essentially a Bcast. Bcast gave incorrect results here for some reason
		for (int proc = 1; proc < size; proc++){
			if (rank == 0){
				MPI_Send(Ldata, N*N, MPI_INT, proc, 0, MPI_COMM_WORLD);
				MPI_Send(Adata, N*N, MPI_INT, proc, 0, MPI_COMM_WORLD);
			}
			else{
				if (rank == proc){
					MPI_Recv(Ldata, N*N, MPI_INT, 0, 0, MPI_COMM_WORLD, &stat);
					MPI_Recv(Adata, N*N, MPI_INT, 0, 0, MPI_COMM_WORLD, &stat);
				}
			}
		}
		//MPI_Bcast(Adata, N*N, MPI_INT, 0, MPI_COMM_WORLD);
		//MPI_Bcast(Ldata, N*N, MPI_INT, 0, MPI_COMM_WORLD);
		t += dt;
	}
	gettimeofday(&end,NULL);
	//print_particle_positions(x);
	time_taken = (end.tv_sec - start.tv_sec)*100000L + (end.tv_usec - start.tv_usec);
	time_in_usec = (double)time_taken;
	time_taken_in_seconds = time_in_usec/div;
	time_taken = (end.tv_sec - start.tv_sec)*100000L + (end.tv_usec - start.tv_usec);
	printf("a single simulation of %d particles in parallel for %d processors took %lf seconds\n", N, size, time_taken_in_seconds);
	FILE * timeTaken = fopen("timeTaken.txt", "a"); //change w to a here for ensemble
	fprintf(timeTaken, "a single simulation of %d particles in parallel for %d processors took %lf seconds\n", N, size, time_taken_in_seconds);
	fclose(timeTaken);

	free(x);
	free(xdot);
	free(x_double_dot);
	free(A[0]);//might not need this line after free(U[[0])
	free(A);
	free(flags);
	if (rank == 0){
		free(z);
		free(f);
		free(w);
		free(wr);
		free(wi);
		free(a);
		free(vl);
		free(vr);
		free(pos);
	}
	MPI_Finalize();	
	return 0;
}
